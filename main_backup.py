from __future__ import print_function
from flask import Flask, send_file, render_template, request, make_response
from flask_basicauth import BasicAuth
#from flask_mail import Mail, Message
from functools import wraps, update_wrapper
from article_maker import make_article
from df_creator import make_df
from xml_generator import xml_filename, nitf_generator
from ftplib import FTP
import time
import os

app = Flask(__name__)

# Login and password.

app.config['BASIC_AUTH_USERNAME'] = 'ntb'
app.config['BASIC_AUTH_PASSWORD'] = 'snilldrapsrobot'
app.config['BASIC_AUTH_FORCE'] = True
"""app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME = 'lasselerviktest@gmail.com',
    MAIL_PASSWORD = 'TestTestTest'
)

mail = Mail(app)"""
basic_auth = BasicAuth(app)

date_today = time.strftime("%d-%m-%Y")
json_filename = 'Drapsstatistikk_' + date_today + "_" + ".json"

def generate_filename(genre):

  prefix = 'Drapsstatistikk_'
  genre_name = ''
  suffix = '.xml'

  if genre == 'lopende':
      genre_name += 'lopende_'
  elif genre == 'slutt':
      genre_name += 'slutt_'
  else: 
      genre_name += 'lang_'

  return prefix + genre_name + date_today + suffix

def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response
        
    return update_wrapper(no_cache, view)

@app.route("/")
@basic_auth.required
def index():
    return render_template("index.html")

@app.route("/drap")
def show_tables():
    df = make_df('Backup_2019')
    return render_template("view.html",tables=[df.to_html(classes='drap')], titles = ['Test1'])

@app.route("/lag_json")
@nocache
def download_json():
    df = make_df('Backup_2019')
    df.to_json(json_filename, force_ascii=False, orient='index')
    return send_file(json_filename, as_attachment=True, mimetype='json')

"""@app.route("/send_nhg")
def send_ngh():
    msg = Message("Hello",
                  sender="lasselerviktest@gmail.com",
                  recipients=["lasse.lervik@ntb.no"])
    mail.send(msg)
    return "Alt gikk fint."""

@app.route("/robot")
@nocache
def lag_faktaboks():
    fakta_slutt = make_article('slutt')
    fakta_lopende = make_article('lopende')
    fakta_lang = make_article('lang')
    return render_template("robot.html", fakta_aar_slutt = fakta_slutt, fakta_aar_lopende = fakta_lopende, fakta_aar_lang = fakta_lang)

# The three following routes generate .XML files in the NITF format
# and send them to NTB's FTP server to be imported automatically into Superdesk

@app.route("/ftp_lopende")
def lopende_ftp():
    nitf_generator(make_article('lopende'), generate_filename('lopende'))
    ftp = FTP('ftp.ntb.superdesk.pro')
    ftp.login('ftp-ntb', 'SuhA59788HM33uS')
    ftp.cwd('/test')

    with open (generate_filename('lopende')):
       ftp.storbinary('STOR ' + generate_filename('lopende'), open(generate_filename('lopende'), 'rb'))
    ftp.quit()
    return 'Filen er lastet opp til FTP.'

@app.route("/ftp_aarsslutt")
def slutt_ftp():
    nitf_generator(make_article('slutt'), generate_filename('slutt'))
    ftp = FTP('ftp.ntb.superdesk.pro')
    ftp.login('ftp-ntb', 'SuhA59788HM33uS')
    ftp.cwd('/test')

    with open (generate_filename('slutt')):
       ftp.storbinary('STOR ' + generate_filename('slutt'), open(generate_filename('slutt'), 'rb'))
    ftp.quit()
    return 'Filen er lastet opp til FTP.'


@app.route("/ftp_lang")
def lang_ftp():
    nitf_generator(make_article('lang'), generate_filename('lang'))
    ftp = FTP('ftp.ntb.superdesk.pro')
    ftp.login('ftp-ntb', 'SuhA59788HM33uS')
    ftp.cwd('/test')

    with open (generate_filename('lang')):
       ftp.storbinary('STOR ' + generate_filename('lang'), open(generate_filename('lang'), 'rb'))
    ftp.quit()
    return 'Filen er lastet opp til FTP.'

@app.route("/2018")
def index_2018():
    return render_template("2018.html")

@app.route("/2017")
def index_2017():
    return render_template("2017.html")

@app.route("/2016")
def index_2016():
    return render_template("2016.html")

@app.route("/2015")
def index_2015():
    return render_template("2015.html")

@app.route("/2014")
def index_2014():
    return render_template("2014.html")


if __name__ == "__main__":
   app.run()
   