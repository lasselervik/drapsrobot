// Initialize Firebase

var config = {
    apiKey: "AIzaSyAnvq2LA31lFNPnjAtSRhjBP5DCQEfs7pM",
    authDomain: "robot-admin-ntb.firebaseapp.com",
    databaseURL: "https://robot-admin-ntb.firebaseio.com",
    projectId: "robot-admin-ntb",
    storageBucket: "robot-admin-ntb.appspot.com",
    messagingSenderId: "269678204528"
};


firebase.initializeApp(config);

function signInWithEmailPassword() {
    var email = "lasse.lervik@ntb.no";
    var password = "LangtOgGodtPassordForAdminBruker";
    // [START auth_signin_password]
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        // ...
        })
        .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        });
    // [END auth_signin_password]
    }

signInWithEmailPassword();

const dbRef = firebase.database().ref();

const usersRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}`); 

function openForm() {
	document.getElementById("inputForm").style.display = "block";
  }
  
  function closeForm() {
	document.getElementById("inputForm").style.display = "none";
  }

readUserData(); 

// --------------------------
// READ
// --------------------------

function readUserData() {

	const userListUI = document.getElementById("userList");

	usersRef.on("value", snap => {

		userListUI.innerHTML = ""

		snap.forEach(childSnap => {

			let key = childSnap.key,
				value = childSnap.val()
  			
			let $li = document.createElement("li");

			// edit icon
			let editIconUI = document.createElement("span");
			editIconUI.class = "edit-user";
			editIconUI.innerHTML = "<br> ✎";
			editIconUI.setAttribute("userid", key);
			editIconUI.addEventListener("click", editButtonClicked)

			// delete icon
			let deleteIconUI = document.createElement("span");
			deleteIconUI.class = "delete-user";
			deleteIconUI.innerHTML = " ☓";
			deleteIconUI.setAttribute("userid", key);

			deleteIconUI.addEventListener("click", deleteButtonClicked);
			
			// Homicide main list
			$li.innerHTML = value.Dato + ": " + value.Kommune;
			$li.append(editIconUI);
			$li.append(deleteIconUI);

			$li.setAttribute("user-key", key);
			$li.addEventListener("click", userClicked)
			userListUI.append($li);

 		});


	})

}

function userClicked(e) {


		var userID = e.target.getAttribute("user-key");

		const userRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}/` + userID);
		const drapInfoUI = document.getElementById("drapInfo");

		userRef.on("value", snap => {

			drapInfoUI.innerHTML = ""

			snap.forEach(childSnap => {
		
				var $p = document.createElement("p");
				$p.innerHTML = childSnap.key  + ": " + "<i>" + childSnap.val() + "</i>";
				drapInfoUI.append($p);
			})

		});
	

}


// --------------------------
// ADD
// --------------------------

const addUserBtnUI = document.getElementById("add-user-btn");
addUserBtnUI.addEventListener("click", addUserBtnClicked)

function addUserBtnClicked() {


	const usersRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}`);

	const addUserInputsUI = document.getElementsByClassName("user-input");

 	// this object will hold the new user information
    let newUser = {};

    // loop through View to get the data for the model 
    for (let i = 0, len = addUserInputsUI.length; i < len; i++) {

        let key = addUserInputsUI[i].getAttribute('data-key');
        let value = addUserInputsUI[i].value;
        newUser[key] = value;
    }

	usersRef.push(newUser, function(){
   console.log("Successfully added new entry");
	})
	
closeForm();

}

// --------------------------
// DELETE
// --------------------------

function deleteButtonClicked(e) {

		e.stopPropagation();

		var userID = e.target.getAttribute("userid");

        const userRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}/` + userID);

		
		if(confirm("Er du sikker på at du vil slette oppføringen?") == true) { userRef.remove();
		}
		
		else return false;
}


// --------------------------
// EDIT
// --------------------------

function editButtonClicked(e) {
	
	document.getElementById('edit-user-module').style.display = "block";

	//set user id to the hidden input field
	document.querySelector(".edit-userid").value = e.target.getAttribute("userid");

	const userRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}/` + e.target.getAttribute("userid"));

	// set data to the user field
	const editUserInputsUI = document.querySelectorAll(".edit-user-input");


	userRef.on("value", snap => {

		for(var i = 0, len = editUserInputsUI.length; i < len; i++) {

			var key = editUserInputsUI[i].getAttribute("data-key");
					editUserInputsUI[i].value = snap.val()[key];
		}

	});


	const saveBtn = document.querySelector("#edit-user-btn");
	saveBtn.addEventListener("click", saveUserBtnClicked)
}


function saveUserBtnClicked(e) {
 
	const userID = document.querySelector(".edit-userid").value;
	const userRef = dbRef.child(`/drapsrobot/Backup_${brukerAar}/` + userID);

	var editedUserObject = {}

	const editUserInputsUI = document.querySelectorAll(".edit-user-input");

	editUserInputsUI.forEach(function(textField) {
		let key = textField.getAttribute("data-key");
		let value = textField.value;
  		editedUserObject[textField.getAttribute("data-key")] = textField.value
	});

	userRef.update(editedUserObject);

	document.getElementById('edit-user-module').style.display = "none";

}

new Sortable(userList, {
    animation: 150,
    ghostClass: 'blue-background-class'
});