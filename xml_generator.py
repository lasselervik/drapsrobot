import time
import os
import lxml.builder    
from ftplib import FTP
from lxml import etree


year = time.strftime("%Y")
date_today = time.strftime("%d-%m-%Y")
timestamp = time.strftime("%Y.%m.%d %H:%M:%S")
ntb_dato = time.strftime("%d.%m.%Y %H:%M")
ntb_utdato = time.strftime("%d.%m.%Y")
date_issue = time.strftime("%Y-%m-%dT%H:%M:%S%z")
time_pubdata = time.strftime("%Y%m%dT%H%M%S%z")

def xml_filename():
  prefix = 'Drapsstatistikk_'
  date_today = time.strftime('%Y_%m_%d')
  suffix = '.xml'
  return prefix + date_today + suffix

# Superdesk specific metadata attributes

ntb_editor = 'Superdesk'
ntb_bilder_antall = '0'
ntb_tjeneste = 'Nyhetstjenesten'
ntb_stikkord = 'drap-statistikk'
tema = 'drap-statistikk'
ntb_id = 'test'
ntb_distrokode = 'ALL'
ntb_kanal = 'A'
ntb_iptc_seq = '1'

tobj_prop_type = 'Faktaboks'
tobj_subj_mat = 'Drap'
tobj_subj_refnum = '02001001'
#tobj_subj_mat2 = 'Vold' <- Usikkert om hele linja kan fjernes
tobj_subj_refnum2 = '02001006'
tobj_subj_refnum3 = '02000000'
tobj_subj_type = 'Kriminalitet og rettsvesen'

ntb_urgency = '5'

keyword_key = 'drapsrobot'

title = f'Fakta om drapssaker i Norge i {year}'

# Making the XML tree itself.

def nitf_generator(liste, filename):

  nitf = etree.Element('nitf', version='-//IPTC//DTD NITF 3.6//EN', baselang='nb-NO')
  head = etree.SubElement(nitf, 'head')

  head.append(etree.fromstring(f'<title>Drapsstatistikk for {year}</title>'))
  head.append(etree.fromstring(f'<meta content="{ntb_bilder_antall}" name="NTBBilderAntall"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_editor}" name="NTBEditor"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_tjeneste}" name="NTBTjeneste"/>'))
  head.append(etree.fromstring(f'<meta content="{xml_filename()}" name="filename"/>'))
  head.append(etree.fromstring(f'<meta content="{timestamp}" name="timestamp"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_dato}" name="ntb-dato"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_utdato}" name="NTBUtDato"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_stikkord}" name="NTBStikkord"/>'))
  head.append(etree.fromstring(f'<meta content="{tema}" name="subject"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_id}" name="NTBID"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_distrokode}" name="NTBDistribusjonsKode"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_kanal}" name="NTBKanal"/>'))
  head.append(etree.fromstring(f'<meta content="{ntb_iptc_seq}" name="NTBIPTCSequence"/>'))

  tobject = etree.SubElement(head, 'tobject')
  tobject.set('tobject.type', 'Innenriks')
  tobject.append(etree.fromstring(f'<tobject.property tobject.property.type="{tobj_prop_type}"/>'))
  tobject.append(etree.fromstring(f'<tobject.subject tobject.subject.matter="{tobj_subj_mat}" tobject.subject.refnum="{tobj_subj_refnum}"/>'))
  #tobject.append(etree.fromstring(f'<tobject.subject tobject.subject.matter="{tobj_subj_mat2}" tobject.subject.refnum="{tobj_subj_refnum2}"/>'))
  tobject.append(etree.fromstring(f'<tobject.subject tobject.subject.refnum="{tobj_subj_refnum3}" tobject.subject.type="{tobj_subj_type}"/>'))

  docdata = etree.SubElement(head, 'docdata')
  docdata.append(etree.fromstring(f'<urgency ed-urg="{ntb_urgency}"/>'))
  docdata.append(etree.fromstring(f'<date.issue norm="{date_issue}"/>')) #må avklare format
  docdata.append(etree.fromstring('<doc-id id-string="NTB1948ea60-391b-11e9-9beb-833308887203" regsrc="NTB"/>')) #må avklare hva dette er
  docdata.append(etree.fromstring('<ed-msg info=""/>'))

  keylist = etree.SubElement(docdata, 'key_list')
  keylist.append(etree.fromstring(f'<keyword key="{keyword_key}"/>'))

  docdata.append(etree.fromstring('<du-key key="drap-statistikk" version="1"/>'))
  docdata.append(etree.fromstring('<evloc state-prov="Norge" county-dist="Riksnyheter"/>'))
  docdata.append(etree.fromstring('<evloc county-dist="Norge"/>'))

  head.append(etree.fromstring(f'<pubdata date_publication="{time_pubdata}" item_length="1000" unit_of_measure="character"/>'))

  body = etree.SubElement(nitf, 'body')
  bodyhead = etree.SubElement(body, 'body.head')

# Hedline is the Article title
  hedline = etree.SubElement(bodyhead, 'hedline')
  hedline.append(etree.fromstring(f'<hl1>{title}</hl1>'))

  bodyhead.append(etree.fromstring('<byline>NTB</byline>'))
  distributor = etree.SubElement(bodyhead, 'distributor')
  distributor.append(etree.fromstring('<org>NTB</org>'))
  bodyhead.append(etree.fromstring('<dateline>Oslo</dateline>'))

# The article itself.

  bodycontent = etree.SubElement(body, 'body.content')
  
# Function takes a list of paragraphs and iterates over it to generate
# the article itself.

  def article_text(list):
    for i in list: 
      print(bodycontent.append(etree.fromstring(f'<p class="txt-ind">{i}</p>')))

  article_text(liste)

  bodyend = etree.SubElement(body, 'body.end')
  tagline = etree.SubElement(bodyend, 'tagline')
  tagline.append(etree.fromstring('<a href="mailto:innenriks.vaktsjef@ntb.no">Levert av NTBs automatiserte artikkeltjeneste</a>'))


# Generates an .XML file in the same directory as the script

  def create_xml(filename):
    with open(filename, 'wb') as f:
      f.write(etree.tostring(nitf, pretty_print=True))

  create_xml(filename)