from __future__ import print_function
import pandas as pd
import time
from df_creator import make_df

def make_article(main_list):
  
  YEAR = time.strftime("%Y")
  df = make_df('Backup_2022')

# Converts integers to numeral words if they're larger than 12,
# per Norwegian typing standards.

  def num_conv(n):
    
    dictionary = {0: 'ingen', 1:'ett', 2:'to', 3:'tre', 4:'fire', 5:'fem', 6:'seks',
    7:'sju', 8:'åtte', 9:'ni', 10:'ti', 11:'elleve', 12:'tolv', 'i ett':'i én'}

    if n < 13:
        return dictionary.get(n) 
    return int(n)

  def homicide_cases_count():

    if df['Definert som drap'].value_counts()['Ja']:
      homicide_cases_total = df['Definert som drap'].value_counts()['Ja']

    if homicide_cases_total == 1:
      return f"én drapssak"
    else:
      return f"{num_conv(homicide_cases_total)} drapssaker"



  def victims_total():
      total = df['Mannlige ofre'].sum() + df['Kvinnelige ofre'].sum() + \
      df['Ukjent kjønn ofre'].sum()
      if total == 1:
        return "ett offer"
      elif total < 13:
        return f"{num_conv(total)} ofre" 
      return f"{int(total)} ofre"


# Counts the number of victims in the database that are under 18 years of age but
# above 1, since 0 is used for unknown ages. 

  def victims_children():
      source = df['Alder offer']
      count = 0

      for i in source:
        if len(str(i)) >= 3:
          None 
        elif i <= 17 and i >= 1:
              count += 1

      if count > 0 and 13 > count:
        return f'{num_conv(count).capitalize()} av dem var barn.'

      elif count > 12:
        return f'{count} av dem var barn.'

      elif count == 0:
        return 'Ingen av dem var barn.'

      elif count == victims_total():
        return 'Alle ofrene var barn.'

      else: 
        return ''

  def other_cases_total():

    other_cases = df['Definert som drap'].value_counts()['Nei']

    if other_cases == None:
      return ''
    else:
      return f"I tillegg var det {num_conv(other_cases)} saker knyttet til dødelig vold."

  def victims_count():

    VICTIMS_MEN = int(df['Mannlige ofre'].sum())
    VICTIMS_WOMEN = int(df['Kvinnelige ofre'].sum())

    if VICTIMS_MEN == None:
      VICTIMS_MEN = 0

    if VICTIMS_WOMEN == None:
      VICTIMS_WOMEN = 0

    if VICTIMS_MEN < 13 and VICTIMS_WOMEN == 1:
      return num_conv(VICTIMS_MEN) + ' av de avdøde var menn og én var kvinne'

    elif VICTIMS_WOMEN < 13 and VICTIMS_MEN == 1:
      return num_conv(VICTIMS_WOMEN) + ' av de avdøde var kvinner og én var mann'

    elif VICTIMS_MEN < 13 and VICTIMS_WOMEN < 13:
      return num_conv(VICTIMS_MEN) + ' av de avdøde var menn og ' + num_conv(VICTIMS_WOMEN) + ' var kvinner'

    elif VICTIMS_MEN > 12 or VICTIMS_WOMEN > 12:
      return str(VICTIMS_MEN) + ' av de avdøde var menn og ' + str(VICTIMS_WOMEN) + ' var kvinner'

    elif VICTIMS_WOMEN == 0 and VICTIMS_MEN < 13:
      return num_conv(VICTIMS_MEN) + ' av de avdøde var menn. Ingen var kvinner'

    

  CHARGED_MEN = df['Pågrepne menn'].sum()
  CHARGED_WOMEN = df['Pågrepne kvinner'].sum()
  CHARGED_TOTAL = int(CHARGED_MEN) + int(CHARGED_WOMEN)

  def charged_gender_highest():
    if CHARGED_MEN > CHARGED_WOMEN:
      return 'menn'
    return 'kvinner'

  def charged_gender_highest_num(): 
      highest = charged_gender_highest()
      if CHARGED_MEN == 0 or CHARGED_WOMEN == 0:
        return 'Alle de'
      elif CHARGED_TOTAL < 13 and highest == 'menn':
        return num_conv(CHARGED_MEN) + ' av de'
      elif CHARGED_TOTAL < 13 and highest == 'kvinner':
        return num_conv(CHARGED_WOMEN) + ' av de'
      elif CHARGED_TOTAL > 12 and highest == 'menn':
        return str(CHARGED_MEN) + ' av de'
      elif CHARGED_TOTAL > 12 and highest == 'kvinner':
        return str(CHARGED_WOMEN) + ' av de'


  def weapon_high():
      weapon = df['Våpen'].value_counts().index.tolist()

      if 'annen' in weapon: 
        weapon.remove('annen')
      if 'ukjent' in weapon: 
        weapon.remove('ukjent')

      return weapon

  def weapon_count():
      counter = df['Våpen'].value_counts().to_dict()

      if 'ukjent' in counter:
        del counter['ukjent']
      if 'annen' in counter:
        del counter['annen']
  
      return [value for value in counter.values()]

  highest_weapon = weapon_high()
  count_weapon = weapon_count()
  
# Forces capitalization of the first letter after a dot

  def last_touch(liste):
      temp_list = []
      for i in liste:
        i = "* " + i[:1].upper() + i[1:]
        temp_list.append(i)
        temp_list = [w.replace('i ett', 'i én') for w in temp_list]
      return temp_list

# Generates articles as a list of fstring paragraphs.

  def lang_fakta(): 
      ingress = [f"Hittil i år har politiet her i landet startet etterforskning av {homicide_cases_count()} med {victims_total()}."]
       
      fakta_liste = df['Informasjon'].tolist()
      
      return ingress + fakta_liste

  def aar_slutt():

      liste = []

      ingress = f"I {YEAR} ble det startet etterforskning av {homicide_cases_count()} med {victims_total()}."
      linje1 = f"{victims_count()}. {victims_children()}\n"
      linje2 = f"{charged_gender_highest_num()} siktede eller tiltalte gjerningspersonene er {charged_gender_highest()}.\n"
      linje3 = f"{highest_weapon[0]} var den vanligste drapsmetoden og ble brukt i {num_conv(count_weapon[0])} av sakene."
      
      liste.extend((ingress, linje1, linje2, linje3))

      return last_touch(liste)

  def aar_lopende():
  
      liste = []

      ingress = f"Hittil i år har politiet her i landet startet etterforskning av {homicide_cases_count()} med {victims_total()}." 
      linje1 = f"{victims_count()}. {victims_children()}\n"
      linje2 = f"{charged_gender_highest_num()} siktede eller tiltalte gjerningspersonene er {charged_gender_highest()}.\n"
      linje3 = f"{highest_weapon[0]} er den vanligste drapsmetoden og er brukt i {num_conv(count_weapon[0])} av sakene."
  
      liste.extend((ingress, linje1, linje2, linje3))
  
      return last_touch(liste)

  if main_list == 'lang':
    return lang_fakta()
  elif main_list == 'slutt':
    return aar_slutt()
  elif main_list == 'lopende':
    return aar_lopende()
  else: 
    raise Exception("Du må oppgi en gyldig liste.")
