import pandas as pd 
from df_creator import make_df

df = make_df('Backup_2022')
pd.set_option('display.max_colwidth', 30)

weapon_count = df['Våpen'].value_counts().tolist()
weapon_name = df['Våpen'].value_counts().index.tolist()

weapon_usage = {k: v for k, v in zip(weapon_count, weapon_name)}


print(f'Vanligste drapsmetoder: \n')
for k, v in weapon_usage.items():
    print(f'{k}: {v} ')

print(weapon_count)
print(weapon_name)
print(weapon_usage.items())
#print(df)